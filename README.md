> **_NOTE:_** This project should make a bug reproducible that appears with the given [browserlist](https://github.com/browserslist/browserslist) configuration. 

## Steps to reproduce
1. Clone the repository
2. `npm i`
3. `npm start`
4. Open [localhost:3000](http://localhost:3000) and open the browser console.

## Expected behaviour
Variable `global` is defined as "hello".

## Actual behaviour
Variable `global` is undefined.
