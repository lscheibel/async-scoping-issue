function App () {
  const global = 'hello'

  console.log('const in global scope', global)

  const condition = true

  const handleClick = async () => {
    if (!condition) {
      const global = 'ciao'
      console.log('const in if', global)
    } else {
      console.log('const in else', global)
    }
  }

  handleClick()

  return (
      <div>
        Check out the console
      </div>
  )
}

export default App
